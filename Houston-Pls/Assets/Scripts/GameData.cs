﻿/*
    {
        radiation = +-n,

        muscle = +-n,

        blood = +-n,

        sight = +-n,

        psych = +-n,

        coordination = +-n,
    }
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{

    // first level (easy)
        // first event
            public static Evento.Answer l1_firstEventAnswer1 = new Evento.Answer() { content = "Make them sit in a horizontal position!", parameterVariation = new GameState() {coordination = -5, blood = -5}, feedback = "Copy & out!" };
            public static Evento.Answer l1_firstEventAnswer2 = new Evento.Answer() { content = "Can you make them lay on their side?", parameterVariation = new GameState() {coordination = -15, blood = -5}, feedback = "I am not convinced, but we can try…" };
            public static Evento.Answer l1_firstEventAnswer3 = new Evento.Answer() { content = "Give them a painkiller for the time being, they'll feel better in no time!", parameterVariation = new GameState() {coordination = -10, blood = -5 }, feedback = "I don’t think that will change anything…" };
            public static Evento l1_firstEvent = new Evento() {logText = "Strong acceleration during launch!", imageId = 0, content = new List<string>() { "Houston, some people are feeling dizzy and are having problem with coordination during the lauch", "What should we do?" }, answers = new Evento.Answer[] { l1_firstEventAnswer1, l1_firstEventAnswer2, l1_firstEventAnswer3 } } ;
        // second event
            public static Evento.Answer l1_secondEventAnswer1 = new Evento.Answer() { content = "Go as fast as you can but care to keep a straight direction!", parameterVariation = new GameState() {radiation = -10, coordination = -10}, feedback = "I don't thing going faster might help..." };
            public static Evento.Answer l1_secondEventAnswer2 = new Evento.Answer() { content = "They are unavoidable, pick the less radiative spot and procede", parameterVariation = new GameState() {radiation = -10}, feedback = "I hear you" };
            public static Evento.Answer l1_secondEventAnswer3 = new Evento.Answer() { content = "There is an area without radiation but filled with small debris..", parameterVariation = new GameState() {psych = -5}, feedback = "I will inform you of the results" };
            public static Evento l1_secondEvent = new Evento() {logText = "Crossing Van Allen band", imageId = 0, content = new List<string>() { "We are crossing the Van Allen bands and our devices are detecting lots of incoming radiation!", "Can we protect ourselves from this?" }, answers = new Evento.Answer[] { l1_secondEventAnswer1, l1_secondEventAnswer2, l1_secondEventAnswer3 } } ;
        // third event
            public static Evento.Answer l1_thirdEventAnswer1 = new Evento.Answer() { content = "There is also another part in sector G6 to repair... Let's work on that as well", parameterVariation = new GameState() {sight = -10}, feedback = "Uff..." };
            public static Evento.Answer l1_thirdEventAnswer2 = new Evento.Answer() { content = "It's worse if we don't addres this, take a coffee and we'll be in touch", parameterVariation = new GameState() {sight = -5, psych = -5}, feedback = "Let's try..." };
            public static Evento.Answer l1_thirdEventAnswer3 = new Evento.Answer() { content = "Don't worry and take a nap, our engineers are already working on this", parameterVariation = new GameState() {coordination = +5}, feedback = "Thank you Houston" };
            public static Evento l1_thirdEvent = new Evento() {logText = "Equipment damaged", imageId = 0, content = new List<string>() { "One of the on board equipment has been damaged!", "We are really exhausted and stressed, everything we tried didn't work and have made us tired" }, answers = new Evento.Answer[] { l1_thirdEventAnswer1, l1_thirdEventAnswer2, l1_thirdEventAnswer3 } } ;
        // fourth event
            public static Evento.Answer l1_fourthEventAnswer1 = new Evento.Answer() { content = "Make them have a drink and calm down", parameterVariation = new GameState() {psych = -5, sight = +5}, feedback = "That'll do" };
            public static Evento.Answer l1_fourthEventAnswer2 = new Evento.Answer() { content = "Good call, make them talk with a psychologist", parameterVariation = new GameState() {psych = +10}, feedback = "It's probably the best solution possible" };
            public static Evento.Answer l1_fourthEventAnswer3 = new Evento.Answer() { content = "Take a nap, this'll solve itself", parameterVariation = new GameState() {psych = -5, coordination = +5}, feedback = "No effects detected" };
            public static Evento l1_fourthEvent = new Evento() {logText = "Fight with crewmate", imageId = 0, content = new List<string>() { "Houston, two of my team have been fighting", "now everything is under control but I don't want to run any risk in the future, what should I do?" }, answers = new Evento.Answer[] { l1_fourthEventAnswer1, l1_fourthEventAnswer2, l1_fourthEventAnswer3 } } ;
        // fifth event
            public static Evento.Answer l1_fifthEventAnswer1 = new Evento.Answer() { content = "It might be! Do some physiotherapy for your body", parameterVariation = new GameState() {muscle = +5, psych = +5}, feedback = "Thanks for the support" };
            public static Evento.Answer l1_fifthEventAnswer2 = new Evento.Answer() { content = "Just go to sleep, don't worry tomorrow this problem is going to be solved", parameterVariation = new GameState() {muscle = -15, psych = +5}, feedback = "... But the crew is still in a bad state!" };
            public static Evento.Answer l1_fifthEventAnswer3 = new Evento.Answer() { content = "You risk some atrophy to your muscle, do a workout immediately!", parameterVariation = new GameState() {psych = +5, muscle = -5}, feedback = "Excellent choice!" };
            public static Evento l1_fifthEvent = new Evento() {logText = "Sharing same space for a long time", imageId = 0, content = new List<string>() {"There has been an issue with a room and it has been shut down until repairement are completed", "We've been living in a small space for days now, is this a problem?" }, answers = new Evento.Answer[] { l1_fifthEventAnswer1, l1_fifthEventAnswer2, l1_fifthEventAnswer3 } } ;
        // sixth event
            public static Evento.Answer l1_sixthEventAnswer1 = new Evento.Answer() { content = "Don't panic, reach the safety equipment, we'll handle the maneuver", parameterVariation = new GameState() {muscle = -5}, feedback = "You've saved us!" };
            public static Evento.Answer l1_sixthEventAnswer2 = new Evento.Answer() { content = "Relax, have a coffee", parameterVariation = new GameState() {coordination = -15}, feedback = "Oh, I see... We're all gonna DIE" };
            public static Evento.Answer l1_sixthEventAnswer3 = new Evento.Answer() { content = "Go to the shielded room", parameterVariation = new GameState() {coordination = -5, muscle = -5}, feedback = "That will not do the job for sure!" };
            public static Evento l1_sixthEvent = new Evento() {logText = "Ordinary maneuver needed", imageId = 0, content = new List<string>() { "Houston, we might have a problem", "We've detected some small asteroids ahead of us, PLS help!" }, answers = new Evento.Answer[] { l1_sixthEventAnswer1, l1_sixthEventAnswer2, l1_sixthEventAnswer3 } } ;
        // seventh event
            public static Evento.Answer l1_seventhEventAnswer1 = new Evento.Answer() { content = "Do some excersize, it'll go away", parameterVariation = new GameState() {blood = -5, muscle = +5}, feedback = "Your solution is not working" };
            public static Evento.Answer l1_seventhEventAnswer2 = new Evento.Answer() { content = "You probably have an accumulation of blood, don't worry, use the decopmressurizant equipment!", parameterVariation = new GameState() {blood = +10}, feedback = "That really improved my health!" };
            public static Evento.Answer l1_seventhEventAnswer3 = new Evento.Answer() { content = "Assume some anti-thrombosis drugs and it'll pass", parameterVariation = new GameState() {psych = -5}, feedback = "Hmmm..." };
            public static Evento l1_seventhEvent = new Evento() {logText = "Thrombosis risk", imageId = 0, content = new List<string>() { "I've been feeling strange in my legs", "They are hurting, what is happening to me?" }, answers = new Evento.Answer[] { l1_seventhEventAnswer1, l1_seventhEventAnswer2, l1_seventhEventAnswer3 } } ;
        // eight event
            public static Evento.Answer l1_eightEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l1_eightEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l1_eightEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l1_eightEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l1_eightEventAnswer1, l1_eightEventAnswer2, l1_eightEventAnswer3 } } ;

    // second level (hard)
        // first event
            public static Evento.Answer l2_firstEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_firstEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_firstEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_firstEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_firstEventAnswer1, l2_firstEventAnswer2, l2_firstEventAnswer3 } } ;
        // second event
            public static Evento.Answer l2_secondEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_secondEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_secondEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_secondEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_secondEventAnswer1, l2_secondEventAnswer2, l2_secondEventAnswer3 } } ;
        // third event
            public static Evento.Answer l2_thirdEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_thirdEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_thirdEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_thirdEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_thirdEventAnswer1, l2_thirdEventAnswer2, l2_thirdEventAnswer3 } } ;
        // fourth event
            public static Evento.Answer l2_fourthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_fourthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_fourthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_fourthEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_fourthEventAnswer1, l2_fourthEventAnswer2, l2_fourthEventAnswer3 } } ;
        // fifth event
            public static Evento.Answer l2_fifthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_fifthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_fifthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_fifthEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_fifthEventAnswer1, l2_fifthEventAnswer2, l2_fifthEventAnswer3 } } ;
        // sixth event
            public static Evento.Answer l2_sixthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_sixthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_sixthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_sixthEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_sixthEventAnswer1, l2_sixthEventAnswer2, l2_sixthEventAnswer3 } } ;
        // seventh event
            public static Evento.Answer l2_seventhEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_seventhEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_seventhEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_seventhEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_seventhEventAnswer1, l2_seventhEventAnswer2, l2_seventhEventAnswer3 } } ;
        // eight event
            public static Evento.Answer l2_eightEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_eightEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_eightEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_eightEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_eightEventAnswer1, l2_eightEventAnswer2, l2_eightEventAnswer3 } } ;
        // nineth event
            public static Evento.Answer l2_ninethEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_ninethEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_ninethEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_ninethEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_ninethEventAnswer1, l2_ninethEventAnswer2, l2_ninethEventAnswer3 } } ;
        // tenth event
            public static Evento.Answer l2_tenthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_tenthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_tenthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_tenthEvent = new Evento() {logText = "esempio", imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_tenthEventAnswer1, l2_tenthEventAnswer2, l2_tenthEventAnswer3 } } ;


    // levels 
        public static Level firstLevel = new Level() { events = new List<Evento>() { l1_firstEvent, l1_secondEvent, l1_thirdEvent, l1_fourthEvent, l1_fifthEvent, l1_sixthEvent, l1_seventhEvent, l1_eightEvent }, nextEvent = new List<float>() { 5, 35, 35, 35, 30, 25, 20, 15}, timer = 240f };
        public static Level secondLevel = new Level() { events = new List<Evento>() { l2_firstEvent, l2_secondEvent, l2_thirdEvent, l2_fourthEvent, l2_fifthEvent, l2_sixthEvent, l2_seventhEvent, l2_eightEvent, l2_ninethEvent, l2_tenthEvent}, nextEvent = new List<float>() { 5, 35, 35, 30, 25, 25, 25, 20, 15, 10 }, timer = 240f };
        
}