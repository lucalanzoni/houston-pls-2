﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GraphicManager graphicManager;
    public SoundManager soundManager;

    public GameObject startScene;
    public GameObject editScene;
    public GameObject gameScene;
    public GameObject endingScene;

    public GameObject answerA;
    public GameObject answerB;
    public GameObject answerC;

    public Text parameters;

    public Image mainScreen;
    public Text conversationText;

    public GameObject[] logObject;

    public Text timerText;

    public GameObject coffe;

    public int selectedLevelIndex;

    Queue<Evento> eventQueue = new Queue<Evento>();
    Queue<float> eventCountdownQueue = new Queue<float>();

    public List<Evento> dispachedEvents = new List<Evento>();

    public Evento selectedEvent = null;
    int selectedEventTextIndex = 0;


    public GameState gameState;

    public float timer = 0;
    public bool isTimeFlowing = false;

    public float nextDispatch;

    public bool doubleTime = false;
    public bool halfTime = false;

    // Start is called before the first frame update
    void Start()
    {
        graphicManager.setLogObj(logObject);

        gameState = new GameState() { blood = 30, sight = 30, radiation = 30, psych = 30, muscle = 30, coordination = 30 };
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimeFlowing)
        {
            if (timer > 0)
            {
                if (isLosing())
                {
                    loseLevel();
                }

                if (doubleTime)
                {
                    timer -= Time.deltaTime * 2;
                    nextDispatch -= Time.deltaTime * 2;
                }
                else if (halfTime)
                {
                    timer -= Time.deltaTime / 2;
                    nextDispatch -= Time.deltaTime / 2;
                }
                else
                {
                    timer -= Time.deltaTime;
                    nextDispatch -= Time.deltaTime;
                } 

                timerText.text = ((int)timer).ToString();
                if (nextDispatch <= 0)
                {
                    Dispatch();
                    if (eventCountdownQueue.Count > 0)
                    {
                        nextDispatch = eventCountdownQueue.Dequeue();
                    }
                }
            }
            else
            {
                Debug.Log("Time run out!");
                endLevel();
            }
        }

        // da spostare fuori l'update
        graphicManager.ShowParameters(parameters, gameState);

    }

    public void loadScene(GameObject scene)
    {
        startScene.SetActive(false);
        editScene.SetActive(false);
        gameScene.SetActive(false);
        endingScene.SetActive(false);

        scene.SetActive(true);

        // if (scene = game scene....
    }

    // carico un livello
    public void loadLevel(int levelIndex)
    {
        selectedLevelIndex = levelIndex;
        loadScene(gameScene);

        Level level;
        switch (levelIndex)
        {
            case 1:
            default:
                level = GameData.firstLevel;
                break;
        }

        timer = level.timer;
        foreach(var evento in level.events)
        {
            eventQueue.Enqueue(evento);
        }

        foreach (var countdown in level.nextEvent)
        {
            eventCountdownQueue.Enqueue(countdown);
        }

        // carico il primo dispatch
        nextDispatch = eventCountdownQueue.Dequeue();

        startGame();
    }

    public void retryLevel()
    {
        loadScene(gameScene);
        loadLevel(selectedLevelIndex);
        startGame();
    }

    // avvio il gioco
    public void startGame()
    {
        graphicManager.showDefault();
        graphicManager.ShowLog(dispachedEvents);
        isTimeFlowing = true;

        selectedEvent = null;
        selectedEventTextIndex = 0;
        doubleTime = false;
        halfTime = false;
    }

    //fine livello
    public void endLevel()
    {
        isTimeFlowing = false;
        graphicManager.ShowVictoryScreen();
        loadScene(endingScene);
    }

    public void doubleTimeActive()
    {
        halfTime = false;
        doubleTime = !doubleTime;
    }

    public void coffeActive()
    {
        doubleTime = false;
        halfTime = !halfTime;
    }

    // mando l'evento sul log
    public void Dispatch()
    {
        if (eventCountdownQueue.Count == 0) return;
        dispachedEvents.Add(eventQueue.Dequeue());

        // se il log è pieno
        if (dispachedEvents.Count > logObject.Length)
        {
            // misso il primo evento
            missEvent(dispachedEvents[0]);
            dispachedEvents.RemoveAt(0);
        }

        graphicManager.ShowLog(dispachedEvents);
    }

    // seleziono l'evento dal log
    public void runEvent(int logIndex)
    {
        if (logIndex < dispachedEvents.Count)
        {
            if (selectedEvent == null)
            {
                selectedEvent = dispachedEvents[logIndex];
                dispachedEvents.RemoveAt(logIndex);
                showEvent();
                graphicManager.ShowLog(dispachedEvents);
            }
        }
    }

    // mostro l'evento selezionato
    public void showEvent()
    {
        selectedEventTextIndex = -1;
        showNextDialog();

        //selectedEvent.imageId
        
    }

    public void showNextDialog()
    {
        if (selectedEvent == null) return;

        if (selectedEvent.content.Count > selectedEventTextIndex + 1) selectedEventTextIndex += 1;
        if (selectedEvent.content.Count == selectedEventTextIndex + 1)
        {
            conversationText.text =
                selectedEvent.content[selectedEventTextIndex] +
                "\nA. " + selectedEvent.answers[0].content +
                "\nB. " + selectedEvent.answers[1].content +
                "\nC. " + selectedEvent.answers[2].content;
        }
        else
        {
            conversationText.text = selectedEvent.content[selectedEventTextIndex];
        }
    }

    public void slectAnswer(int answerIndex)
    {
        if (selectedEvent == null) return;

        if (selectedEvent.content.Count == selectedEventTextIndex + 1)
        {
            if (selectedEvent.answers.Length > answerIndex)
            {
                gameState.parameterVariation(selectedEvent.answers[answerIndex].parameterVariation);
                conversationText.text = selectedEvent.answers[answerIndex].feedback;
                selectedEvent = null;
            }
        }
    }

    public void missEvent(Evento evento)
    {
        var missState = new GameState() { blood = -10, sight = -10, radiation = -10, psych = -10, muscle = -10, coordination = -10 };
        gameState.parameterVariation(missState);
    }

    // se due parametri arrivano a zero hai perso
    private bool isLosing()
    {
        int count = 0;
        if (gameState.blood <= 0) count++;
        if (gameState.coordination <= 0) count++;
        if (gameState.muscle <= 0) count++;
        if (gameState.psych <= 0) count++;
        if (gameState.radiation <= 0) count++;
        if (gameState.sight <= 0) count++;

        if (count >= 2) return true;
        else return false;
    }

    public void loseLevel()
    {
        isTimeFlowing = false;
        graphicManager.ShowLosingScreen();
        loadScene(endingScene);
    }

    public void exitGame()
    {
        Application.Quit();
    }
}
