﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicManager : MonoBehaviour
{
    public GameManager gameManager;

    private GameObject[] logObject;

    public GameObject victoryMenu;
    public GameObject loseMenu;
    public Image endingScreen;

    public Sprite victoryBg;
    public Sprite loseBg;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void showDefault()
    {
        //gameManager.parameters.text = 
        gameManager.conversationText.text = "";

    }

    public void setLogObj(GameObject[] goArray)
    {
        logObject = goArray;
    }

    public void ShowLog(List<Evento> eventList)
    {
        for (int i=0; i < logObject.Length; i++)
        {
            if (eventList.Count > i) logObject[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = eventList[i].logText;
            else logObject[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = "";
        }
    }

    public void ShowParameters(Text parameters, GameState gameState)
    {
        parameters.text = "radiation = " + gameState.radiation +
            "\n\nblood = " + gameState.blood +
            "\n\ncoordination = " + gameState.coordination +
            "\n\nmuscle = " + gameState.muscle +
            "\n\npsych = " + gameState.psych +
            "\n\nsight = " + gameState.sight;
    }

    public void ShowVictoryScreen()
    {
        endingScreen.sprite = victoryBg;

        victoryMenu.SetActive(true);
        loseMenu.SetActive(false);
    }

    public void ShowLosingScreen()
    {
        endingScreen.sprite = loseBg;

        victoryMenu.SetActive(false);
        loseMenu.SetActive(true);
    }
}
